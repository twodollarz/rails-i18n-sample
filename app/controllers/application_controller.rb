class ApplicationController < ActionController::Base
  protect_from_forgery
  require 'http_accept_language'
end
